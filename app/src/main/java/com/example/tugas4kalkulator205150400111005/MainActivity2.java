package com.example.tugas4kalkulator205150400111005;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;

public class MainActivity2 extends AppCompatActivity {

    TextView buttonR;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        buttonR = (TextView) findViewById(R.id.button_hasil);
        String hasil = getIntent().getStringExtra("hasil");
        buttonR.setText(hasil);
    }
}